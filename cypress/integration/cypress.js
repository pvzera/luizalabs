/// <reference types="cypress" />

const getIframeDocument = () => {
    return cy
        .get('iframe[id="result"]')
        .its('0.contentDocument').should('exist')
}

const getIframeBody = () => {
    return getIframeDocument()
        .its('body').should('not.be.undefined')
        .then(cy.wrap)
}

const pointerEvent = {
    force: true,
    pointerType: 'touch'
}

describe("LuizaLabs Test", () => {

    it('Long Click', () => {
        cy.visit('https://codepen.io/choskim/pen/RLYebL')
        getIframeBody().find('div.square')
            .should('have.css', "height", '90px')
            .should('have.css', "width", '90px')
            .trigger('pointerdown', 'center', pointerEvent)
        cy.wait(1500)
        getIframeBody().find('div.square.expand')
            .trigger('pointerup', 'center', pointerEvent)
    })

    it('Assert width and height', () => {

        getIframeBody().find('div.square.expand')
            .should('have.css', "height", '225px')
            .should('have.css', "width", '225px')

    })

})